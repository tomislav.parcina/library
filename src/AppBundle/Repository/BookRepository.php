<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Book;
use AppBundle\Form\Model\SearchParameters;
use Doctrine\ORM\EntityRepository;

class BookRepository extends EntityRepository
{
    /**
     * Vraća knjige koje zadovoljavaju zadane kriterije.
     * 
     * @param SearchParameters $books Parametri za pretragu
     * 
     * @return array
     */
    public function findByParameters(SearchParameters $searchParameters)
    {
        $dql = 'SELECT b FROM AppBundle:Book AS b';
        $where = [];
        $parameters = [];
        if (!empty($searchParameters->bookName)){
            $where[] = ' b.bookName LIKE :book_name ';
            $parameters['book_name'] = '%'.$searchParameters->bookName.'%';
        }
        if (!empty($searchParameters->bookAuthor)){
            $where[] = ' b.bookAuthor LIKE :book_author ';
            $parameters['book_author'] = '%'.$searchParameters->bookAuthor.'%';
        }
        if (!empty($searchParameters->bookIsbn10)){
            $where[] = ' b.bookIsbn10 LIKE :isbn_10 ';
            $parameters['isbn_10'] = '%'.$searchParameters->bookIsbn10.'%';
        }
        if (!empty($searchParameters->bookIsbn13)){
            $where[] = ' b.bookIsbn13 LIKE :isbn_13 ';
            $parameters['isbn_13'] = '%'.$searchParameters->bookIsbn13.'%';
        }
        if ($where){
            $dql .= " WHERE " . implode(' AND ', $where) . " ";
        }
        
        return $this->getEntityManager()
            ->createQuery($dql)
            ->setParameters($parameters)
            ->getResult();
    }
}
