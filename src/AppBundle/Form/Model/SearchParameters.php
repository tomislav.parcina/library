<?php
namespace AppBundle\Form\Model;

class SearchParameters
{
	public $id;
	public $bookName = "";
	public $bookAuthor = "";
	public $bookIsbn10 = "";
	public $bookIsbn13 = "";
}
