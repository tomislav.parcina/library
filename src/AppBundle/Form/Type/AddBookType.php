<?php
 
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;

use AppBundle\Entity\Book;

class AddBookType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('bookIsbn10', TextType::class, ['required' => false])
            ->add('searchGoogleApi', SubmitType::class, [])
            ->add('bookName', TextType::class, ['required' => false])
            ->add('bookAuthor', TextType::class, ['required' => false])
            ->add('bookIsbn13', TextType::class, ['required' => false])
            ->add('cover', FileType::class, ['required' => false])
            ->add('dodaj', SubmitType::class, []);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'method' => 'post',
            'data_class' => Book::class,
            'csrf_protection' => false,
        ]);
    }
}
