<?php
namespace AppBundle\Form\Type;

use AppBundle\Form\Model\SearchParameters;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchParametersType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('bookName', TextType::class, [
                'required' => false, 
                'label' => false,
                'invalid_message' => 'Naziv mora biti unesen u formatu: text'
            ])
            ->add('bookAuthor', TextType::class, [
                'required' => false, 
                'label' => false,
                'invalid_message' => 'Pisac mora biti unesen u formatu: text'
            ])
            ->add('bookIsbn10', TextType::class, [
                'required' => false,
                'label' => false,
                'invalid_message' => 'ISBN 10 mora biti unesen u formatu: text'
            ])
            ->add('bookIsbn13', TextType::class, [
                'required' => false,
                'label' => false,
                'invalid_message' => 'ISBN 13 mora biti unesen u formatu: text'
            ])
       ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'method' => 'get',
            'data_class' => SearchParameters::class,
            'csrf_protection' => false,
        ]);
    }
}
