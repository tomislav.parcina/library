<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Book;
use AppBundle\Form\Model\RentBook;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RentBookType extends AbstractType 
{

    public function buildForm(FormBuilderInterface $builder, array $options, int $bookId)
    {
        $builder->add(
            'bookId', 
            HiddenType::class, 
            [
                'required' => true,
                'label' => false,
                'data' => $bookId,
                'invalid_message' => 'ID knjige mora biti unesen!',
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'method' => 'put',
            'data_class' => RentBook::class,
            'csrf_protection' => false,
        ]);
    }
}
