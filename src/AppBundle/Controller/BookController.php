<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Book;
use AppBundle\Form\Model\AddBook;
use AppBundle\Form\Model\RentBook;
use AppBundle\Form\Model\SearchParameters;
use AppBundle\Form\Type\AddBookType;
use AppBundle\Form\Type\SearchParametersType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BookController extends Controller
{
    /** 
     * @Route("/admin/add", name="AppBundle_Book_add")
     *
     * @param Request $request Podaci o knjizi
     * 
     * @return Response Stranica za dodavanje knjiga u bazu
     */
    public function bookAddAction(Request $request)
    {
        $book = new Book();
        $form = $this->createForm(AddBookType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $book = $form->getData();
            $file = $book->getCover();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $file->move(
                $this->getParameter('cover_directory'),
                $fileName
            );

            $book->setCover($fileName);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($book);
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Knjiga je uspješno unesena!'
            );

            return $this->redirectToRoute('AppBundle_Book_add');
        }
        return $this->render('AppBundle:Book:bookAdd.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /** 
     * @Route("/admin/api/{isbn10}", name="AppBundle_Admin_api")
     * 
     * @link https://developers.google.com/books/docs/overview Dokumentacija od API-ja.
     * 
     * @param string $isbn10 ISBN10 oznaka knjige
     * 
     * @return JsonResponse Podatake o knjizi
     */
    public function searchForBookOnGoogleApi(string $isbn10)
    {
        $googleBooksJson = file_get_contents("https://www.googleapis.com/books/v1/volumes?q=isbn:" . $isbn10);
        $googleBook = (json_decode($googleBooksJson));

        foreach ( $googleBook->items as $item )
        {
            $googleBookName = $item->volumeInfo->title;
            $googleBookAuthor = $item->volumeInfo->authors[0];
            $googleBookIsbn13 = $item->volumeInfo->industryIdentifiers[0]->identifier;

            $response = new JsonResponse();
            $response->setData(array(
                'book_name' => $googleBookName,
                'book_author' => $googleBookAuthor,
                'book_isbn_13' => $googleBookIsbn13,
            ));

            return $response;
        }
    }

    /** 
     * @Route("/book/search", name="AppBundle_Book_search")
     *
     * @param Request $request Parametri za pretragu
     *
     * @return Response A Response instance
     */
    public function bookSearchAction(Request $request)
    {
        $form = $this->get('form.factory')->createNamed(null, SearchParametersType::class, new SearchParameters());
        $form->handleRequest($request);

        if($form->isSubmitted()) {
            $books = $this->get('app.book_repository')->findByParameters($form->getData());
        } else {
            $books = $this->get('app.book_repository')->findAll();
        }

        return $this->render('AppBundle:Book:bookList.html.twig', [
            'books' => $books,
            'form' => $form->createView(),
        ]);
    }

    /** 
     * Akcija koja servira naslovnu sliku knjige.
     * Ako slika ne postoji, servira se no-image.jpg slika.
     * 
     * @Route("/book/image/{cover}", name="AppBundle_Book_cover")
     * 
     * @param string $cover Naziv datoteke - slika naslovnice
     *
     * @return BinaryFileResponse Datoteku - sliku naslovnice
     */
    public function coverAction(string $cover)
    {
        $imagePath = __DIR__.'/../Resources/images/book/'.$cover;
        if (!file_exists($imagePath)) {
            $imagePath = __DIR__.'/../Resources/images/book/no-image.jpg';
        }

        $response = new BinaryFileResponse($imagePath);
        $response->headers->set('Content-Type', 'image/jpeg');

        return $response;
    }

    /**
     * @Route("/user/{id}", name="AppBundle_Book_book") 
     * 
     * @param Request $request Parametri za pretragu knjiga
     * @param string $id ID knjige
     * 
     * @throws createNotFoundException ako knjiga s ID-jem iz parametra nije pronađena
     *
     * @return array Book objekata
     */
    public function bookDetailView(Request $request, $id)
    {
        $book = $this->get('app.book_repository')->findOneById($id);
    
        if (!$book) {
            throw $this->createNotFoundException(sprintf('Book with id "%s" not found.', $id));
        }

        $rentBook = new RentBook();
        $rentBook->bookId=$id;

        $form = $this->createFormBuilder($rentBook)
            ->add('bookId', HiddenType::class)
            ->getForm();

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            if (null == $book->getRent()) {
                $user = $this->getUser();
                $userId = $user->getId();
                $book->setRent($userId);
                $notice="posuđena!";
            } else {
                $book->setRent(null);
                $notice="vraćena!";
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($book);
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Knjiga je uspješno ' . $notice
            );
            
            return $this->redirectToRoute('AppBundle_Book_book', ['id' => $id]);
        }

        return $this->render('AppBundle:Book:bookPresentation.html.twig', [
            'book' => $book,
            'form' => $form->createView(),
        ]);
    }
}
