<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WelcomeController extends Controller
{
    /**
     * @Route("/", name="AppBundle_Welcome_homepage")
     * 
     * @return Response Naslovnica
     */
    public function homepageAction(Request $request)
    {
        return $this->render('AppBundle:Welcome:homepage.html.twig');
    }
}
