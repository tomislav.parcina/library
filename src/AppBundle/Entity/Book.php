<?php
 
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BookRepository")
 * @ORM\Table(name="book")
 */
class Book
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100, name="title")
     *
     * @Assert\NotBlank(message="Molimo Vas da unesete naziv knjige.")
     * @Assert\Length(
     *     min = 1,
     *     max = 100,
     *     minMessage = "Molimo Vas da unesete naziv knjige koji sadrži barem jedan znak.",
     *     maxMessage = "Molimo Vas da unesete naziv knjige koji je kraći od 100 znakova.",
     * )
     */
    protected $bookName;

    /**
     * @ORM\Column(type="string", length=200, name="author")
     *
     * @Assert\NotBlank(message="Molimo Vas da unesete ime pisca.")
     * @Assert\Length(
     *     min = 1,
     *     max = 200,
     *     minMessage = "Molimo Vas da unesete ime pisca koje sadrži barem jedan znak.",
     *     maxMessage = "Molimo Vas da unesete ime pisca koje je kraće od 200 znakova.",
     * )
     */
    protected $bookAuthor;

    /**
     * @ORM\Column(type="string", length=10, name="isbn10")
     * 
     * @Assert\NotBlank(message="Molimo Vas da unesete točno 10 znakova za ISBN 10.", groups={"registration"})
     * @Assert\Isbn(
     *     type = "isbn10",
     *     message = "Niste unijeli ispravnu ISBN 10 oznaku."
     * )
     */    
    protected $bookIsbn10;

    /**
     * @ORM\Column(type="decimal", length=13, name="isbn13")
     *
     * @Assert\NotBlank(message="Molimo Vas da unesete točno 13 znamenki za ISBN 13.")
     * @Assert\Isbn(
     *     type = "isbn13",
     *     message = "Niste unijeli ispravnu ISBN 13 oznaku."
     * )
     */    
    protected $bookIsbn13;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Molimo Vas da odaberete naslovnicu knjige.")
     * @Assert\File(
     *     maxSize = "1024k",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/png"},
     *     mimeTypesMessage = "Molimo Vas da odaberete naslovnicu knjige u JPG, JPEG ili PNG formatu, manju od 1.024 KB."
     * )
     */
    protected $cover;

    /** @ORM\Column(type="integer", length=10) */
    protected $rent;

    public function getId()
    {
        return $this->id;
    }

    public function getBookName()
    {
        return $this->bookName;
    }

    public function setBookName($bookName)
    {
        $this->bookName = $bookName;
    }

    public function getBookAuthor()
    {
        return $this->bookAuthor;
    }

    public function setBookAuthor($bookAuthor)
    {
        $this->bookAuthor = $bookAuthor;
    }

    public function getBookIsbn10()
    {
        return $this->bookIsbn10;
    }

    public function setBookIsbn10($bookIsbn10)
    {
        $this->bookIsbn10 = $bookIsbn10;
    }

    public function getBookIsbn13()
    {
        return $this->bookIsbn13;
    }

    public function setBookIsbn13($bookIsbn13)
    {
        $this->bookIsbn13 = $bookIsbn13;
    }

    public function getCover()
    {
        return $this->cover;
    }

    public function setCover($cover)
    {
        $this->cover = $cover;
        return $this;
    }

    public function getRent()
    {
        return $this->rent;
    }

    public function setRent($rent)
    {
        $this->rent = $rent;
    }
}
