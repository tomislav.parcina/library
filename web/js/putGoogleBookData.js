$(document).ready(function() {
   	$("#add_book_searchGoogleApi").on("click", function(e){
   		e.preventDefault();
   		var isbn10 = $("#add_book_bookIsbn10").val();
		$.getJSON( "api/" + isbn10, function( json ) {
	   		$("#add_book_bookName").val(json.book_name);
		   	$("#add_book_bookAuthor").val(json.book_author);
		   	$("#add_book_bookIsbn13").val(json.book_isbn_13);
		});
	});
});
