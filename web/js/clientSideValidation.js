$(document).ready(function() {
    // validacija naziva knjige
    $('#bookName').on('change', function(){
        var bookName = $(this);
        var bookNameLength = bookName.val().length;
        var submitBtn = $('#filtriraj');

        if(( bookNameLength > 0 && bookNameLength < 3 ) || bookNameLength > 50) {
            bookName.addClass('errorField');
            submitBtn.addClass('disabled');
        } else {
            bookName.removeClass('errorField');
            submitBtn.removeClass('disabled');
        }
    });

    // validacija pisca
    $('#bookAuthor').on('change', function(){
        var bookAuthor = $(this);
        var bookAuthorLength = bookAuthor.val().length;
        var submitBtn = $('#filtriraj');

        console.log(bookAuthorLength);

        if(( bookAuthorLength > 0 && bookAuthorLength < 3 ) || bookAuthorLength > 50) {
            bookAuthor.addClass('errorField');
            submitBtn.addClass('disabled');
        } else {
            bookAuthor.removeClass('errorField');
            submitBtn.removeClass('disabled');
        }
    });

    // validacija ISBN10 oznake
    $('#bookIsbn10').on('change', function(){
        var bookIsbn10 = $(this);
        var bookIsbn10Length = bookIsbn10.val().length;
        var submitBtn = $('#filtriraj');

        if(( bookIsbn10Length > 0 && bookIsbn10Length != 10 )) {
            bookIsbn10.addClass('errorField');
            submitBtn.addClass('disabled');
        } else {
            bookIsbn10.removeClass('errorField');
            submitBtn.removeClass('disabled');
        }
    });

    // validacija ISBN13 broja
    $('#bookIsbn13').on('change', function(){
        var bookIsbn13 = $(this);
        var bookIsbn13Length = bookIsbn13.val().length;
        var submitBtn = $('#filtriraj');

        if(( bookIsbn13Length > 0 && bookIsbn13Length != 13 )) {
            bookIsbn13.addClass('errorField');
            submitBtn.addClass('disabled');
        } else {
            bookIsbn13.removeClass('errorField');
            submitBtn.removeClass('disabled');
        }
    });

    $('#filtriraj').on('click', function(e){
        e.preventDefault();
        var form = $('#form_book_search');

        if (!$(this).hasClass('disabled'))
            form.submit();
        else
            return false;
    });
});
